package com.guarani.services;

import com.guarani.domains.Company;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 *
 * @author Gustavo
 */
public class ReceitawsAPI {
    
    
    private String getCompanyDetails(String cnpj) throws MalformedURLException, IOException {
        
        String request ="https://www.receitaws.com.br/v1/cnpj/".concat(cnpj);
        
        URL url = new URL(request);
        URLConnection conn = url.openConnection();
        BufferedReader buffer = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String linha;
        while ((linha = buffer.readLine()) != null) {

            System.out.println(linha);

        }
        return linha;
    }
}
